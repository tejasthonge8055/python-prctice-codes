


'''
Nested for Patterns

Program1 :

Rows = 3
* * *
* * *
* * *
'''

for i in range(0,3):

    for j in range(0,3):
        print( "*" ,end=" ")

    print()
