#row String 

str1 = 'Welcome to IIT '
print(str1)

str2 = 'Welcome to \tcore2web \npune'
print(str2)

#it print all the string without print \n \t

#but we want to print row string 
#just add r befor the string

str3 =r'Chatrapati \t shivaji \n maharaj'
print(str3)
