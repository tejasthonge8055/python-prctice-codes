

#Accaseing the string

'''thir are two way 
    1.index
    2.slicing
'''
#1.indexing 
str1 ="core2web"
print(str1)   #core2web
print(str1[3]) #e

#2.Slicing

print(str1[::])
print(str1[1:7:])
print(str1[::2])
print(str1[-1:-8:])
print(str1[-1:-9:-1])

#cheak all the output of the above slicing code
